install:
	npm install -g csso-cli

dist/gleam.min.css: src/gleam.css
	csso src/gleam.css -o dist/gleam.min.css

gleam: dist/gleam.min.css

all: install gleam
